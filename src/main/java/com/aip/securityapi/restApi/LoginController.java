package com.aip.securityapi.restApi;


import com.aip.securityapi.dto.AuthencationResponse;
import com.aip.securityapi.dto.RequestAuth;
import com.aip.securityapi.dto.RequestRegister;
import com.aip.securityapi.entity.User;
import com.aip.securityapi.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class LoginController {

    @Autowired
    private AuthService authService;
    @PostMapping("/register")
    public ResponseEntity<AuthencationResponse> register(
            @RequestBody RequestRegister requestRegister){

        return ResponseEntity.ok(authService.register(requestRegister));
    }

    @PostMapping("/login")
    public ResponseEntity<AuthencationResponse> register(
            @RequestBody RequestAuth requestAuth){

        return ResponseEntity.ok(authService.login(requestAuth));

    }
}
