package com.aip.securityapi.restApi;

import com.aip.securityapi.dao.UserDao;
import com.aip.securityapi.dto.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserDao userDao;

    @GetMapping("/list")
    public ResponseEntity<BaseResponse> getUserList(){
        return ResponseEntity.ok().body(
                BaseResponse.builder().data(userDao.findAll()).build()
        );
    }
}
