package com.aip.securityapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse {
    private String responseCode;
    private String responseMessage;
    private Object data;

    public BaseResponse(String responseCode, String responseMessage){
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;

    }
}
