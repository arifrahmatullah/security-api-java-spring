package com.aip.securityapi.dto;

import lombok.Data;

@Data
public class RequestRegister {
    private String firstname;
    private String lastname;
    private String email;
    private String password;
}
