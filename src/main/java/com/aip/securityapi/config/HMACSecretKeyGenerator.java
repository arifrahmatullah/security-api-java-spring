package com.aip.securityapi.config;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;

public class HMACSecretKeyGenerator {
    public static SecretKey generateHMACSecretKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("HmacSHA256"); // You can choose a different algorithm if needed
        return keyGen.generateKey();
    }
}
