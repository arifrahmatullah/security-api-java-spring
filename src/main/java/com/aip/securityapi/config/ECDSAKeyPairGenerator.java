package com.aip.securityapi.config;

import java.security.*;
import java.security.spec.ECGenParameterSpec;

public class ECDSAKeyPairGenerator {
    public static KeyPair generateECDSAKeyPair() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        ECGenParameterSpec ecSpec = new ECGenParameterSpec("secp256r1"); // Specify the curve
        keyPairGenerator.initialize(ecSpec, new SecureRandom());
        return keyPairGenerator.generateKeyPair();
    }
}
