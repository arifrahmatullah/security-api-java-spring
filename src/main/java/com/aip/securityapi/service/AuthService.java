package com.aip.securityapi.service;

import com.aip.securityapi.dao.UserDao;
import com.aip.securityapi.dto.AuthencationResponse;
import com.aip.securityapi.dto.RequestAuth;
import com.aip.securityapi.dto.RequestRegister;
import com.aip.securityapi.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private UserDao userDao;

    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    public AuthService(PasswordEncoder passwordEncoder, JwtService jwtService, AuthenticationManager authenticationManager) {
        this.passwordEncoder = passwordEncoder;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
    }

    public AuthencationResponse register(RequestRegister request){
        var user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(User.Role.ADMIN)
                .build();

        System.out.println("Test : " + user);
        userDao.save(user);



        return AuthencationResponse.builder()
                .token(jwtService.generateToken(user)).build();
    }

    public AuthencationResponse login(RequestAuth requestAuth){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        requestAuth.getEmail(),
                        requestAuth.getPassword()
                )
        );
        var user = userDao.findByEmail(requestAuth.getEmail())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        var refreshToken = jwtService.generateToken(user);
        return AuthencationResponse.builder()
                .token(jwtService.generateToken(user)).build();
    }
}
